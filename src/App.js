import { RouterApp } from "./routers/RouterApp";
import 'bulma/css/bulma.css';
import "@fortawesome/fontawesome-free/js/all";
import axios  from "axios";
import { AuthContextProvider } from "./contexts/authContext";

axios.interceptors.request.use(function(config){
  //console.log(config);
  config.url= `${process.env.REACT_APP_API_BASE_URL}${config.url}`;
  console.log(`${process.env.REACT_APP_API_BASE_URL}${config.url}`);

  return config;
})

function App() {
  return (
    <AuthContextProvider>
      <RouterApp/>
    </AuthContextProvider>
  );
}

export default App;
