import React, { createContext, useReducer } from 'react';
import authReducer from '../reducer/authReducer';

export const AuthContext = createContext();

const initialState = {
    logged: false
}

export const AuthContextProvider = props =>{

    const [state,dispatch]= useReducer(authReducer,initialState );

    return(
        <AuthContext.Provider
            value={
                {
                    state,
                    dispatch
                }
            }
            >
            {props.children}
        </AuthContext.Provider>
    )

}
