import React, { createContext, useReducer } from 'react';
import { OBTENER_PRODUCTOS, REGISTRAR_PRODUCTO,OBTENER_PRODUCTO, MODIFICAR_PRODUCTO, ELIMINAR_PRODUCTO} from '../const/actionTypes';
import productoReducer from '../reducer/productoReducer';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import Swal from 'sweetalert2'

export const ProductContext = createContext();

export const ProductContextProvider = props=>{

    const initialState={
        productList: [],
        productoActual: null
    }

    const [state,dispatch]= useReducer(productoReducer,initialState );

    const obtenerProductos = async()=>{

        try{

            const resultado = await axios.get('/totalproduct');
            console.log(resultado);

            dispatch({
                type: OBTENER_PRODUCTOS,
                payload: resultado.data
            })

        }catch(error){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'No se pudo obtener la lista de productos'
            })
            console.log(error);
        }

        
    }

    const registrarProducto = async producto =>{

        try{

            const resultado = await axios.post('/product',producto);
            console.log(resultado);

            let productoNuevo={
                ...producto,
                idproducto:  uuidv4()
            }
    
            dispatch({
                type: REGISTRAR_PRODUCTO,
                payload: productoNuevo
            })

            Swal.fire({
                icon: 'success',
                title: 'Correcto',
                text: 'Producto registrado correctamente'
            })

        }catch(error){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'No se pudo registrar el producto'
            })
            console.log(error);
        }

    }

    const obtenerProducto = async producto =>{

        try{

            let productoEncontrado={
                nombre: '',
                descripcion:'',
                precio_unidad:0,
                cantidad: 0,
                nombre_m:'',
                rank:0,
        
            }

            if(producto !== null){
                const resultado = await axios.get(`/product/${producto.nombre}`);
                console.log(resultado);
                console.log(resultado.data);
                productoEncontrado=resultado.data;
                
            }
            else{
                productoEncontrado=producto;
            }
            
            

            dispatch({
                type: OBTENER_PRODUCTO,
                payload: productoEncontrado
            })


        }catch(error){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'No se pudo obtener el producto'
            })
            console.log(error);
        }
        
    }

    const actualizarProducto= async producto => {

        try{

            const resultado = await axios.put(`/productp/${producto.nombre}`,producto);
            console.log(resultado);
            console.log("Producto obtenido");
            console.log(producto);

            dispatch({
                type: MODIFICAR_PRODUCTO,
                payload: producto,
            })

            
            Swal.fire({
                icon: 'success',
                title: 'Correcto',
                text: 'Producto modificado correctamente'
            })

        }catch(error){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'No se pudo modificar el producto'
            })
            console.log(error);
        }

    }

    const eliminarProducto = async producto =>{

        console.log(producto);

        try{

            
            Swal.fire({
                
                title: '¿Desea continuar?',
                text: 'Se eliminara el producto seleccionado',
                icon: 'question',
                showCancelButton: true,
                confirmButtonText: 'Si, eliminar'
                
            }).then(async(result)=>{
                if(result.value){
                    const resultado = await axios.put(`/productd/${producto.nombre}`);
                    console.log(resultado);

                    dispatch({
                        type: ELIMINAR_PRODUCTO,
                        payload: producto.idproducto
                    })

                          
                    Swal.fire({
                        icon: 'success',
                        title: 'Correcto',
                        text: 'Producto eliminado'
                    })


                }
            })

            

        }catch(error){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'No se pudo eliminar el producto'
            })
            console.log(error);
        }
        
        
    }

    return(
        <ProductContext.Provider
        value={{
            productList: state.productList,
            productoActual: state.productoActual,

            obtenerProductos,
            registrarProducto,
            obtenerProducto,
            actualizarProducto,
            eliminarProducto,
        }}

    >
        {props.children}

        </ProductContext.Provider>
    )
}



