import { ELIMINAR_PRODUCTO, MODIFICAR_PRODUCTO, OBTENER_PRODUCTO, OBTENER_PRODUCTOS, REGISTRAR_PRODUCTO } from "../const/actionTypes";


export default (state,action) => {

    switch (action.type){
        case OBTENER_PRODUCTOS:
            return {
                ...state,
                productList: action.payload
            };
        case REGISTRAR_PRODUCTO:
            return{
                ...state,
                productList:[...state.productList,action.payload]
                
            };

        case OBTENER_PRODUCTO:
            return{
                ...state,
                productoActual: action.payload
                        
            };
    
        case MODIFICAR_PRODUCTO:
            return{
                ...state,
                productList: state.productList.map(
                    producto => producto.idproducto === action.payload.idproducto ? action.payload : producto
                )       
            };
    
        case ELIMINAR_PRODUCTO:
            return{
                ...state,
                productList: state.productList.filter(producto => producto.idproducto !== action.payload)  
            };        

        default:
            return state;
    }
}
