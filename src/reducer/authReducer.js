import { AUTH_LOGIN, AUTH_LOGOUT } from "../const/actionTypes";

const initialState = {
    logged: false,
    user: null
}

const authReducer = (state=initialState,action) => {

    switch (action.type){
        case AUTH_LOGIN:
            return {
                ...state,
                logged: true,
                user: { ...action.payload  }
            };
        case AUTH_LOGOUT:
            return{
                ...state,
                logged: false,
                user: null
            };
        default:
            return state;
    }

}

export default authReducer
