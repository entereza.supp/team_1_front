import React ,{ useContext, useState, useEffect }from 'react';
import { ModalContext } from '../../contexts/modalContext';
import { ProductContext } from '../../contexts/productContext';

const FormProducto = () => {

    const {setShowModal} =useContext(ModalContext);
    const{registrarProducto,actualizarProducto,productoActual,obtenerProducto}= useContext(ProductContext);

    //para vaciar el formulario
    const productDefault={
        nombre: '',
        descripcion:'',
        precio_unidad:0,
        cantidad: 0,
        nombre_m:'',
        rank:0,

    }

    const [producto, setProducto]=useState(productDefault);

    const [mensaje, setMensaje]=useState(null);

    useEffect(()=>{
      if(productoActual!== null){
        setProducto({
          ...productoActual,
          descripcion: productoActual.descripcion ? productoActual.descripcion: '',
          nombre_m: productoActual.nombre_m ? productoActual.nombre_m: '',
          rank: productoActual.rank ? productoActual.rank: 0,
        })
      }else{
        setProducto(productDefault);
      }
    },[productoActual])

    const handleChange= e=>{
        setProducto({
            ...producto,
            [e.target.name]: e.target.value
        })
    }

    const handleOnSubmit = e =>{
        e.preventDefault();

        //validacion
        if(producto.nombre.trim() ==='' || producto.precio_unidad === 0 || producto.cantidad === 0){
            setMensaje('El nombre, precio, y la cantidad son obligatorios');
            return;
        }

        
        //obtener objeto a enviar

        if(productoActual !== null){
          actualizarProducto(obtenerProductoAEnviar());
        }
        else{
          registrarProducto(obtenerProductoAEnviar());
        }
        


        //cerra y limpiar el modal
        
        cerrarModal();
        
    } 

    const limpiarForm= () => {
        setMensaje(null);
        setProducto(productDefault);
    }

    const cerrarModal= () => {
        limpiarForm();
        setShowModal(false);
        obtenerProducto(null);
    }

    const obtenerProductoAEnviar =()=>{
        //eliminamos los componenetes que no tengan datos
        let productoTemp={...producto};
        if(productoTemp.descripcion==="")delete productoTemp.descripcion;
        if(productoTemp.nombre_m==="")delete productoTemp.nombre_m;
        if(productoTemp.rank==="")delete productoTemp.rank;
        
        return productoTemp;
    }

    return ( 
        <form onSubmit={handleOnSubmit}>

        {mensaje ? <div className="notification is-danger">{mensaje}</div>: null}
            
      <div className="field is-horizontal">
        <div className="field-label is-normal">
          <label className="label">Nombre del Producto</label>
        </div>
        <div className="field-body">
          <div className="field">
            <p className="control is-expanded has-icons-left">
              <input
                autoComplete="off"
                className="input"
                type="text"
                placeholder="Nombre"
                name="nombre"
                value={producto.nombre}
                onChange={handleChange}
              />
              <span className="icon is-small is-left">
              <i className="fas fa-mobile-alt"></i>
              </span>
            </p>
          </div>
          <div className="field">
            <p className="control is-expanded">
              <input
                autoComplete="off"
                className="input"
                type="text"
                placeholder="Descripcion"
                name="descripcion"
                value={producto.descripcion}
                onChange={handleChange}
              />
            </p>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label is-normal">
          <label className="label">Precio Por Unidad</label>
        </div>
        <div className="field-body">
          <div className="field">
            <div className="control is-expanded has-icons-left">
              <input
                autoComplete="off"
                className="input"
                type="number"
                step="any"
                placeholder="Ingrese el precio del producto por unidad"
                name="precio_unidad"
                value={producto.precio_unidad}
                onChange={handleChange}
              />
              <span className="icon is-small is-left">
              <i className="fas fa-dollar-sign"></i>
              </span>
            </div>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label is-normal">
          <label className="label">Cantidad</label>
        </div>
        <div className="field-body">
          <div className="field">
            <div className="control is-expanded has-icons-left">
              <input
                autoComplete="off"
                className="input"
                type="number"
                placeholder="Cantidad o Stock Disponible"
                name="cantidad"
                value={producto.cantidad}
                onChange={handleChange}
              />
              <span className="icon is-small is-left">
              <i className="fas fa-boxes"></i>
              </span>
            </div>
          </div>
        </div>
      </div>


      <div className="field is-horizontal">
        <div className="field-label is-normal">
          <label className="label">Marca</label>
        </div>
        <div className="field-body">
          <div className="field">
            <div className="control is-expanded has-icons-left">
              <input
                autoComplete="off"
                className="input"
                type="text"
                placeholder="Ingrese la marca del producto"
                name="nombre_m"
                value={producto.nombre_m}
                onChange={handleChange}
              />
              <span className="icon is-small is-left">
              <i className="fas fa-building"></i>
              </span>
            </div>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label is-normal">
          <label className="label">Rank</label>
        </div>
        <div className="field-body">
          <div className="field">
            <div className="control is-expanded has-icons-left">
              <input
                autoComplete="off"
                className="input"
                type="number"
                placeholder="Ingrese el rank del producto"
                name="rank"
                value={producto.rank}
                onChange={handleChange}
              />
              <span className="icon is-small is-left">
              <i className="fas fa-sort-numeric-up"></i>
              </span>
            </div>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label">
        </div>
        <div className="field-body">
          <div className="field">
            <div className="control">
              <button type="submit" className="button is-primary mr-1">Guardar</button>
              <button
                type="button"
                className="button"
                onClick={()=> cerrarModal()}
              >Cancelar</button>
            </div>
          </div>
        </div>
      </div>
    </form>
     );
}
 
export default FormProducto;
