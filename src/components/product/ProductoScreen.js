import React from 'react';
import { ProductContextProvider } from '../../contexts/productContext';
import { HelmetSeo } from '../ui/HelmetSeo';
import Layout from '../ui/Layout';
import Modal from '../ui/Modal';
import FormProducto from './FormProducto';
import TableProducto from './TableProduct';
import ToolbarProducto from './ToolbarProducto';

export const ProductoScreen = () => {
  return (
      <>
        <HelmetSeo title="Entereza Products" description="Stewardship product management"/>
        <Layout>
          <ProductContextProvider>
          <div className='panel is-primary'>
          <div className="panel-heading">
              Productos
          </div>
          <div className="box">
              <ToolbarProducto/>
              <TableProducto/>
              
          </div>
      </div>
      <Modal>
        <FormProducto/>
      </Modal>
          </ProductContextProvider>
    </Layout>
      </>
    
  )
};
