import React,{useContext} from 'react';
import { ModalContext } from '../../contexts/modalContext';
import { ProductContext } from '../../contexts/productContext';

const ToolbarProducto = () => {
    const {setModalTitle, setShowModal} =useContext(ModalContext);

    const{obtenerProducto}= useContext(ProductContext);

    const abrirModalCrear=()=>{
        setModalTitle('Registrar nuevo Producto');
        setShowModal(true);
        obtenerProducto(null);
    }

    return (  
        <div className="container">
            <button 
                className="button is-small is-primary"
                onClick={()=>abrirModalCrear()}
                >
             <span className="icon is-small">
                <i className="fas fa-plus"></i>
             </span>
             <span>Registrar nuevo</span>
            </button>
        </div>
    );
}
 
export default ToolbarProducto;

