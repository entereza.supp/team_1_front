import React, { useContext, useEffect } from 'react';
import { ProductContext } from '../../contexts/productContext';
import RowProducto from './RowProduct';

const TableProducto = () => {

    const{productList,obtenerProductos}=useContext(ProductContext);

    useEffect(()=>{
        obtenerProductos();
        // eslint-disable-next-line 
    },[]);

    if(productList.length===0) return <center><p>No existen productos.</p></center>

    return ( 
        <div className="table-container">
                        <table className='table is-hoverable is-fullwidth'>
                            <thead>
                                <tr>
                                    <th>Acciones</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th>Precio Por Unidad</th>
                                    <th>Cantidad</th>
                                    <th>Marca</th>
                                    <th>Rank</th>
                                </tr>
                            </thead> 
                            <tbody>
                                {
                                    productList.map(producto=>(
                                        <RowProducto producto={producto} key={producto.idproducto}/>
                                    ))
                                }
                                
                            </tbody>
                        </table>
                    </div>
     );
}
 
export default TableProducto;

