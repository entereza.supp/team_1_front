import React, { useContext } from 'react';
import { ModalContext } from '../../contexts/modalContext';
import { ProductContext } from '../../contexts/productContext';


const RowProducto = ({producto}) => {
    const{setModalTitle, setShowModal}= useContext(ModalContext);

    const{obtenerProducto,eliminarProducto}=useContext(ProductContext);

    const abrirModalModificarProducto = () =>{
        obtenerProducto(producto);
        setModalTitle('Modificar Producto');
        setShowModal(true);
    }


    return ( 
        <tr>
            <td>
                <button 
                    className="button is-small is-info mr-1" 
                    title='Modificar'
                    onClick={()=>abrirModalModificarProducto()}
                    >
                    <span className="icon is-small">
                    <i className="fas fa-edit"></i>
                    </span>
                </button>
                <button 
                    className="button is-small is-danger" 
                    title='Eliminar'
                    onClick={()=>eliminarProducto(producto)}
                    >
                    <span className="icon is-small">
                    <i className="fas fa-trash-alt"></i>
                    </span>
                </button>
            </td>
            
            <td>{producto.nombre}</td>
            <td>{producto.descripcion}</td>
            <td>{producto.precio_unidad}</td>
            <td>{producto.cantidad}</td>
            <td>{producto.nombre_m}</td>
            <td>{producto.rank}</td>
        </tr>
     );
}
 
export default RowProducto;
