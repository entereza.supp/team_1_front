import { Formik } from 'formik';
import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import Swal from 'sweetalert2';
import { fetchWithoutToken } from '../../../helpers/fetchWithoutToken';
import { validatorRegister } from '../../../helpers/validatorRegister';
import { AlertForm } from '../../ui/AlertForm';
import ProgressBar from "@ramonak/react-progress-bar";
import "./progressbar.css"

export const RegisterForm = () => {

    const [next, setNext] = useState(false);
    const navigate = useNavigate();

    const [progress, setProgress] = useState(true);

    const submitFunction = async(values, { setSubmitting }) => {
        setSubmitting(true);
        try {
            let forCustomer = {
                "nombre": values.name,
                "apellido": values.lastname,
                "email": values.email,
                "telefono": values.telephone,
            }
            let forLogin = {
                "username": values.username,
                "password": values.password,
            }
            
            // const [ responseCustomer, responseLogin ] = await Promise.all([
            //     fetchWithoutToken(`customer`, "POST", forCustomer),
            //     fetchWithoutToken(`login`, "POST", forLogin)
            // ])

            const responseCustomer = await fetchWithoutToken(`customer`, "POST", forCustomer);
            const responseLogin = await fetchWithoutToken(`login`, "POST", forLogin);

            if( responseCustomer.status === 200 && responseLogin.status === 200 ) {
                
                setTimeout(() => {
                    Swal.fire("User Created", "", "success");
                    navigate('/login');
                }, 700)
                
            } else {
                console.log(responseCustomer);
                console.log(responseLogin);
                Swal.fire("Register Error", "", "error");
            }

        } catch (err) {
            console.log(err);
            Swal.fire("Server Error", err.message, "error");
        }
        setSubmitting(false);
    }

    const handleOnNext = () => {
        setProgress(prev=> !prev);
        setNext(prev => !prev);
    }

  return (
    <>
    <ProgressBar baseBgColor="#ffffff" height=".3em" className="wrapper" completed={progress?"50" : "100"} maxCompleted={100} customLabel=" "/>
    
    <div className="login-form">
        <Formik
            initialValues={
                {
                    name: '',
                    lastname: '',
                    telephone: '',
                    email: '',
                    password: '',
                    username: ''
                }
            }
            validate={ validatorRegister }
            onSubmit= {
                submitFunction
            }
        >
            {
                ({
                    values: { name, lastname, telephone, email, password, username },
                    errors,
                    isSubmitting, 
                    handleChange, 
                    handleSubmit,
                    touched
                }) => (
                    <form onSubmit={handleSubmit}>
                        {
                            !next 
                                ? (
                                    <>
                                    <div className="login-form__input">
                                        <label htmlFor="register-name">Name</label>
                                        {
                                            errors.name && touched.name
                                                && <AlertForm error={ errors.name }/>
                                        }
                                        <input className={ errors.name && touched.name? "input-alert": " " } value={name} name="name" onChange={ handleChange } id="register-name" type="text" />
                                    </div>
                                    <div className="login-form__input">
                                        <label htmlFor="register-lastname">Lastname</label>
                                        {
                                            errors.lastname && touched.lastname
                                                && <AlertForm error={ errors.lastname }/>
                                        }
                                        <input className={ errors.lastname && touched.lastname ? "input-alert": " " } value={lastname} name="lastname" onChange={ handleChange } id="register-lastname" type="text" />
                                    </div>
                                    <div className="login-form__input">
                                        <label htmlFor="register-telephone">Telephone</label>
                                        {
                                            errors.telephone && touched.telephone
                                                && <AlertForm error={ errors.telephone }/>
                                        }
                                        <input className={ errors.telephone&& touched.telephone? "input-alert": " " } value={telephone} name="telephone" onChange={ handleChange } id="register-telephone" type="text" />
                                    </div>
                                    <div className="login-form__input">
                                        <label htmlFor="register-email">Email</label>
                                        {
                                            errors.email && touched.email
                                                && <AlertForm error={ errors.email }/>
                                        }
                                        <input className={ errors.email ? "input-alert": " " } value={email} name="email" onChange={ handleChange } id="register-email" type="email" />
                                    </div>
                                    <div onClick={handleOnNext} className="register-next">
                                        next
                                    </div>
                                    </>
                                )
                                : (
                                    <>  
                                        <div className="login-form__input">
                                            <label htmlFor="register-username">Username</label>
                                            {
                                                errors.username && touched.username
                                                    && <AlertForm error={ errors.username }/>
                                            }
                                            <input className={ errors.username? "input-alert": " " } value={username} name="username" onChange={ handleChange } id="register-username" type="text" />
                                        </div>
                                        
                                        <div className="login-form__input">
                                            <label htmlFor="register-pass">Password</label>
                                            {
                                                errors.password && touched.password
                                                    && <AlertForm error={ errors.passwords }/>
                                            }
                                            <input className={ errors.password? "input-alert": " " } value={password} name="password" onChange={ handleChange } id="register-pass" type="password" />
                                        </div>
                                        <div className="login-form__input" style={{visibility: "hidden"}}>
                                            <label>hidden</label>
                                            <input type="text"  disabled/>    
                                        </div>
                                        <div className="login-form__input" style={{visibility: "hidden"}}>
                                            <label>hidden</label>
                                            <input type="text"  disabled/>
                                        </div>
                                        <div onClick={handleOnNext} className="register-next">
                                            back
                                        </div>
                                        
                                    </>
                                )
                        }
                        
                        <button type='submit' disabled={isSubmitting}>
                            Sign up
                        </button>
                        
                    </form>
                )
            }
        </Formik>
    </div>
    </>
  );
};
