import React from 'react';
import { HelmetSeo } from '../../ui/HelmetSeo';
import { RegisterForm } from './RegisterForm';
import { RegisterRedirect } from './RegisterRedirect';

export const RegisterScreen = () => {
  return (
    <>
    <HelmetSeo title="Entereza Register" description="Register and enjoy our tool"/>
    <div className="content-login">
        <div className="login">
            <div className="login-title">
                Sign <span className="text-gradient">Up</span>
            </div>
            <div className="login-info">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates eveniet, sed laboriosam voluptatibus optio qui iusto esse tenetur,  
            </div>

            <RegisterForm/>
            <RegisterRedirect/> 
            
        </div>
        <div className="login-image">
            <div className="login-image__message">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis, esse.
            </div>
        </div>
    </div>
    </>
  )
};
