import React from 'react';
import { Link } from 'react-router-dom';

export const RegisterRedirect = () => {
  return (
    <div className="login-redirect">
        <p>
            Already have an account?
        </p>
        <Link to="/auth/login">
            Log in
        </Link>
    </div>
  );
};
