import React, { useContext } from 'react';
import { Formik } from 'formik';
import { AlertForm } from '../../ui/AlertForm';
import { validatorLogin } from '../../../helpers/validatorLogin';
import { AUTH_LOGIN } from '../../../const/actionTypes';
import { AuthContext } from '../../../contexts/authContext';
import { fetchWithoutToken } from '../../../helpers/fetchWithoutToken';
import Swal from 'sweetalert2';

export const LoginForm = () => {

    const { dispatch} = useContext(AuthContext);
    const submitFunction = async(values, { setSubmitting }) => {
        setSubmitting(true);
        try {
            const response = await fetchWithoutToken(`cliente/${values.username}/${values.password}`)
            const json = await response.json();
            if(json.length === 0) {
                return Swal.fire("Invalid credentials", "", "error");
            }
            
            dispatch({  
                type: AUTH_LOGIN,
                payload: json[0]
            })

        } catch (err) {
            console.log(err);
            Swal.fire("Server Error", err.message, "error");
        }
        setSubmitting(false);
    }

  return (
    <div className="login-form">
        <Formik
            initialValues={{
                username: '',
                password: ''
            }}
            validate={
                validatorLogin
            }
            onSubmit= {
               submitFunction
            }
        >
            {
                ({
                    values:{ username, password }, 
                    errors, 
                    isSubmitting, 
                    handleChange, 
                    handleSubmit
                }) => (
                    <form onSubmit={handleSubmit}>
                        <div className="login-form__input">
                            <label htmlFor="login-email">Username</label>
                            {
                                errors.username &&
                                    <AlertForm error={ errors.username }/> 
                            }
                            <input className={ errors.username ? "input-alert": "" } onChange={handleChange} value={username} name="username" id="login-email" type="text" />
                        </div>
                        <div className="login-form__input">
                            <label htmlFor="login-pass">Password</label>
                            {
                                errors.password &&
                                    <AlertForm error={ errors.password }/> 
                            }
                            <input className={ errors.password ? "input-alert": "" } onChange={handleChange} value={password} name="password" id="login-pass" type="password" />
                        </div>

                        <button type="submit" disabled={isSubmitting}>
                            Log in
                        </button>
                    </form>
                )
            }
        </Formik>
        
    </div>
  );
};
