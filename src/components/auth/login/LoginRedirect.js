import React from 'react';
import { Link } from 'react-router-dom';

export const LoginRedirect = () => {
  return (
    <div className="login-redirect">
        <p>
            Don't have an account?
        </p>
        <Link to="/auth/register">
            Sign up
        </Link>
    </div>
    );
};
