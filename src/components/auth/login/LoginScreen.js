import React from 'react';

import { LoginForm } from './LoginForm';
import '../auth.css';
import { LoginRedirect } from './LoginRedirect';
import { HelmetSeo } from '../../ui/HelmetSeo';

export const LoginScreen = () => {
  return (
    <>
    <HelmetSeo title="Entereza Login" description="Enter and manage your products"/>
    <div className="content-login">
        <div className="login">
            <div className="login-title">
                Log <span className="text-gradient">In</span>
            </div>
            <div className="login-info">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates eveniet, sed laboriosam voluptatibus optio qui iusto esse tenetur, porro perspiciatis laborum quibusdam necessitatibus. 
            </div>

            <LoginForm/>

            <LoginRedirect/>

        </div>
        <div className="login-image">
            <div className="login-image__message">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis, esse.
            </div>
        </div>
    </div>
    </>
    )
};
