import React from 'react';

export const GalleryGridItem = (
    {
        nombre,
        descripcion,
        precio_unidad,
        nombre_m
    }
) => {

    console.log(precio_unidad);
  return (
    <div className="column">
        <div className="card"  style={{ width:"170px", height:"250px" }}>
            <div className="card-image" >
                <figure className="image is-4by3 figure-card">
                    <div className="figure-card__price">
                        {
                            `${precio_unidad}/S`
                        }
                    </div>
                <img src="https://picsum.photos/200/300" alt="imaGe random" />
                </figure>
            </div>
            <div className="card-content" style={{ height: "50%" }}>
                <div className="media" style={{ marginBottom: "0.7em" }}>
                <div className="media-content">
                    <p className="title is-4 card-title">
                        {
                            nombre
                        }
                    </p>
                    <p className="subtitle is-6 card-brand">
                        {
                            nombre_m
                        }
                    </p>
                </div>
                </div>

                <div className="content card-description" >
                    {
                        `${descripcion}descripciondescripciondescripciondescripciondescripciondescripciondescripciondescripciondescripciondescripciondescripcion`
                    }
                </div>
            </div>
        </div>
    </div>
  );
};
