import React from 'react';
import { GalleryGridItem } from './GalleryGridItem';

export const GalleryGrid = ({ products }) => {
  return (
    <div className="box">
        <div className ="columns is-mobile gallery-grid">
            {
                products.map(({ idproducto, ...rest }) => (
                    <GalleryGridItem key={ idproducto } { ...rest }/>
                ))
            }
        </div>
    </div>
          
    );
};
