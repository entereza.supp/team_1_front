import React, { useEffect, useState } from 'react';
import { HelmetSeo } from '../ui/HelmetSeo';
import Layout from '../ui/Layout';
import { fetchWithoutToken } from '../../helpers/fetchWithoutToken';
import { GalleryGrid } from './GalleryGrid';

import './gallery.css'

const GalleryScreen = () => {

    const [products, setProducts] = useState(null);
    
    useEffect(() => {
        fetchWithoutToken("totalproduct")
            .then( res => res.json() )
            .then( res => setProducts( [ ...res ] ))
    
      return () => {
        setProducts([]);
      };
    }, []);
    

    return (
        <>
        <HelmetSeo title="Entereza Dashboard" description="Stewardship product management"/>
        <Layout>
          <div className='panel is-primary'>
          <div className="panel-heading">
              Celulares Mas Vendidos
          </div>
          <div className="box">
          <div className ="columns is-mobile">
            {
                !!products && <GalleryGrid products={ products }/>
            }
        
            </div>
              
          </div>
      </div>
    </Layout>
      </>
      );
}
 
export default GalleryScreen;