import React from 'react';
import { NavLink } from 'react-router-dom';

import './menu.css';


const Menu=()=>{

    return(
        <nav className='panel aside'>
            <p className='panel-heading aside-title'>Menu</p>
            <div className="panel-block aside-items">
                <NavLink to="/dashboard" className={
                    ({isActive}) => (isActive?"is-fullwidth active":"is-fullwidth")
                }>
                    <span className="">
                        Inicio
                    </span>
                </NavLink>
            </div>
            <div className="panel-block aside-items">
                <NavLink to="/products" className={
                    ({isActive}) => (isActive?"is-fullwidth active":"is-fullwidth")
                }>
                    <span className="">
                    Productos
                    </span>
                </NavLink>
            </div>
        </nav>
    )
}
export default Menu;
