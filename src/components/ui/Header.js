import React, { useContext, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import Swal from 'sweetalert2'

import './header.css';
import Logo from '../../assets/logo.png';
import { stringToHslColor } from '../../helpers/stringToHslColor';
import { AuthContext } from '../../contexts/authContext';
import { AUTH_LOGOUT } from '../../const/actionTypes';


const Header=()=>{

    const [navResponsive, setNavResponsive] = useState(false);

    const handleNavResponsive = () => {
        console.log("responsive");
        setNavResponsive(prev => !prev);
    }

    const { dispatch, state:{ user } } = useContext(AuthContext);

    const handleOnLogout = () => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.isConfirmed) {
                dispatch({
                    type: AUTH_LOGOUT
                })
            }
          })
         }

    let avatarColor = stringToHslColor(`${user.nombre} ${user.apellido}`) || "#00b89c";	

    return(
        <div className="navbar-style navbar is-primary  is-flex is-justify-content-space-between is-align-content-center is-flex-direction-row">
            <div className="navbar-brand">
                <Link to="/" className='navbar-item'>
                    <img src={Logo} alt="Entereza"/>
                </Link>
            </div>
            <div className="navbar-options">
                <div className="navbar-options__user">
                    <div
                        className="navbar-options__user-avatar"
                        style={{ backgroundColor: avatarColor }}
                    >
                        {
                            user.nombre.charAt(0).toUpperCase()
                        }
                    </div>
                    <span className="navbar-options__user-name">
                        {
                            `${user.nombre} ${user.apellido}`
                        }
                    </span>
                </div>
                <div onClick={handleOnLogout} className="navbar-options__logout button is-danger is-normal is-rounded">
                    Logout
                </div>
            </div>
            <div onClick={ handleNavResponsive } role="button" className="navbar-burger nav-responsive__button-open" aria-label="menu" aria-expanded="false">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </div>
            <nav className={
                navResponsive?"navbar nav-responsive nav-responsive__initial":"navbar nav-responsive nav-responsive__hidden"
            }> 
                <div  className="nav-reponsive__close">
                    <div onClick={ handleNavResponsive } className="nav-responsive__close-button">
                    <i className="fas fa-times"></i>
                    </div>
                </div>
                <div className="nav-responsive__options aside-items">
                    <NavLink to="/dashboard" className={
                        ({isActive}) => isActive? "nav-responsive__options-item active": "nav-responsive__options-item"
                    }>
                        Inicio
                    </NavLink>
                    <NavLink to="/products" className={
                        ({isActive}) => isActive? "nav-responsive__options-item active": "nav-responsive__options-item"
                    }>
                        Productos
                    </NavLink>
                    <div onClick={ handleOnLogout } className="nav-responsive__options-item has-text-danger">Logout</div>
                </div>
            </nav>
        </div>
    )
}
export default Header;