import React, { useContext,useState} from 'react';
import { ModalContext } from '../../contexts/modalContext';
const Modal = (props) => {
    
    const {showModal,modalTitle} =useContext(ModalContext);

    return ( 
        <div className={`modal ${showModal ? 'is-active' : ''}`}>
            <div className="modal-background">

            </div>
            <div className="modal-card">
                <header className='modal-card-head'>
                    <p className="modal-card-title">
                        {modalTitle}
                    </p>
                    
                </header>
                <section className='modal-card-body'> 
                    {props.children}
                </section>
            </div>
        </div>
     );
}
 
export default Modal;