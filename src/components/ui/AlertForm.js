import React from 'react';

export const AlertForm = ({error}) => {
  return <span className="form-alert">{error}</span>;
};
