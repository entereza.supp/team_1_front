import React from 'react';
import {Helmet} from "react-helmet";

export const HelmetSeo = ({
    title = 'Entereza',
    description = 'Entereza Hackaton',
}) => {
  return (
    <Helmet>
        <title>
            {
                title
            }
        </title>
        <meta name="description" content={ description }/>
    </Helmet>
  );
};
