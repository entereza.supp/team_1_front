import React from 'react';
import { ProductContextProvider } from '../../contexts/productContext';
import { HelmetSeo } from '../ui/HelmetSeo';
import Layout from '../ui/Layout';

export const CardScreen = () => {
  return (
    <>
        <HelmetSeo title="Entereza Card Products" description="Stewardship product management"/>
        <Layout>
            <ProductContextProvider>
                <div className='panel is-primary'>
                    productos
                </div>
            </ProductContextProvider>
        </Layout>
    </>
  );
};
