import React, { useContext } from 'react';
import { 
    BrowserRouter,
    Routes,
    Route,
 } from 'react-router-dom';
import { AuthContext } from '../contexts/authContext';
import { DashboardPrivate } from './DashboardPrivate';
import { DashboardPublic } from './DashboardPublic';
import { PrivateRouter } from './PrivateRouter';
import { PublicRouter } from './PublicRouter';

export const RouterApp = () => {

    const {state:{logged}} = useContext(AuthContext);

  return (
      <BrowserRouter>
        <Routes>

            <Route path="/auth/*" element={
                <PublicRouter authentication={ logged }>
                    <DashboardPublic/>
                </PublicRouter>
            } />
                
            <Route path="/*" element={
                <PrivateRouter authentication={ logged }>
                    <DashboardPrivate/>
                </PrivateRouter>
            } />

                
        </Routes>
      </BrowserRouter>
  );
};
