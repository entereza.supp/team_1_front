import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { LoginScreen } from '../components/auth/login/LoginScreen';
import { RegisterScreen } from '../components/auth/register/RegisterScreen';

export const DashboardPublic = () => {
  return <>
    <Routes>
        <Route path="login" element={ <LoginScreen/> }/>
        <Route path="register" element={ <RegisterScreen/> }/>
    </Routes>
  </>;
};
