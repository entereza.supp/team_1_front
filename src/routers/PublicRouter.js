import React from 'react';
import { Navigate } from 'react-router-dom';

export const PublicRouter = ({
    children,
    authentication
}) => {
  return !authentication
            ? children
            : <Navigate to="/dashboard"/>
};
