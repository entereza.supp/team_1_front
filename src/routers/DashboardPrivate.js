import React from 'react';
import { Route, Routes } from 'react-router-dom';
import GalleryScreen from '../components/gallery/GalleryScreen';
import { ProductoScreen } from '../components/product/ProductoScreen';

export const DashboardPrivate = () => {
  return <>
    <Routes>

        <Route path="products" element={ <ProductoScreen/> }/>

        <Route path="dashboard" element={ <GalleryScreen/> }/>

    </Routes>
  </>;
};
