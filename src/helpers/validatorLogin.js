export const validatorLogin = ({username, password}) => {
    let errors = {};

    if (!username || username.length < 3 || username.length > 30) {
        errors.username = 'Username is required';
    }
    if (!password || password.length < 6) {
        errors.password = 'Password is required';   
    }

    return errors;
}  
