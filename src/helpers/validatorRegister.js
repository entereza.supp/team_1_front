import { regularExpressions } from "./regularExpressions";

export const validatorRegister = ({name, lastname, telephone, email, password, username}) => {
    let errors = {};

    if (!name || name.length < 2 || name.length > 40) {
        errors.name = 'Name is required';
    }
    if(!lastname || lastname.length < 2 || lastname.length > 40) {
        errors.lastname = 'Lastname is required';
    }
    if(!telephone || telephone.length < 5 || telephone.length > 20 || !telephone.match(regularExpressions.telephone)) {
        errors.telephone = 'Telephone is required';
    }
    if(!username || username.length < 2 || username.length > 20){
        errors.username = 'Username is required';
    }     
    if (!email || !email.match(regularExpressions.email) || email.length < 7 || email.length > 255) {
        errors.email = 'Email is required';
    }
    if (!password || password.length < 6 || password.length > 40) {
        errors.password = 'Password is required';   
    }

    return errors;
}  
