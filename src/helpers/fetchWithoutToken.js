const basePath = process.env.REACT_APP_API_URL;
export const fetchWithoutToken = (path, method="GET", data ) => {
    
    const url = `${basePath}/${path}`;
    if(method==="GET") {
        return fetch(url, {
            headers : { 
                'Content-Type': 'application/json',
                'Accept': 'application/json'
               }
        })
    }else {
        return fetch(url, {
            method,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }, 
            body: JSON.stringify(data)
        })
    }

};
